package com.ifmo.markeli.wst.Client;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by markeli on 01.05.17.
 */
public class Arguments
{
	Arguments()
	{
		hostname = "127.0.0.1";
		port = 8080;
		parameters = new HashMap<String, String>();
		parameters.put("isbn", "1");
	}

	public boolean Parse(String[] args)
	{
		for (int i = 0; i < args.length; i++)
		{
			String argument = args[i];
			if ((argument.equals("-h") || argument.equals("--host")) && ++i < args.length )
			{
				hostname = args[i];
			}
			else if ((argument.equals("-p") || argument.equals("--port")) && ++i < args.length)
			{
				port = Integer.valueOf(args[i]);
			}
			else
			{
				int delimiter_pos = argument.indexOf("=");
				String key = argument.substring(0, delimiter_pos);
				String value = argument.substring(delimiter_pos + 1);

				parameters.put(key, value);
			}
		}

		return true;
	}

	public String hostname;
	public int port;
	public Map<String, String> parameters;
}
