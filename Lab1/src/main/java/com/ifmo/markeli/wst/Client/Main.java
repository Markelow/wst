package com.ifmo.markeli.wst.Client;

import com.ifmo.markeli.wst.Client.Generated.Book;
import java.util.List;

/**
 * Created by markeli on 01.05.17.
 */
public class Main
{
	private static void PrintBook(Book book)
	{
		System.out.println("Item:\n" +
				"\tName: " + book.getName() + "\n" +
				"\tAuthor: " + book.getAuthor() + "\n" +
				"\tDescription: " + book.getDescription() + "\n" +
				"\tISBN: " + book.getIsbn() + "\n"
		);
	}

	private static void PrintResult(List<Book> books)
	{
		for (Book book: books)
		{
			PrintEpisode(book);
		}
	}

	public static void main(String[] args)
	{
		try
		{
			Arguments program_argument = new Arguments();
			program_argument.Parse(args);
			Service service = new Service(program_argument.hostname, program_argument.port);
			PrintResult(service.findBy(program_argument.parameters));
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
