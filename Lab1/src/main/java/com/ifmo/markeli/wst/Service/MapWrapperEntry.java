package com.ifmo.markeli.wst.Service;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Created by markeli on 01.05.17.
 */
public class MapWrapperEntry
{
	@XmlAttribute
	public String key;

	@XmlValue
	public String value;
}
