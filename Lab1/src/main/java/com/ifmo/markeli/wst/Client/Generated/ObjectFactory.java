
package com.ifmo.markeli.wst.Client.Generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ifmo.markeli.wst.Client.Generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FindByResponse_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "FindByResponse");
    private final static QName _FindBy_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "FindBy");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ifmo.markeli.wst.Client.Generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindByResponse }
     * 
     */
    public FindByResponse createFindByResponse() {
        return new FindByResponse();
    }

    /**
     * Create an instance of {@link FindBy }
     * 
     */
    public FindBy createFindBy() {
        return new FindBy();
    }

    /**
     * Create an instance of {@link MapWrapperEntry }
     * 
     */
    public MapWrapperEntry createMapWrapperEntry() {
        return new MapWrapperEntry();
    }

    /**
     * Create an instance of {@link TvEpisode }
     * 
     */
    public Book createBook() {
        return new Book();
    }

    /**
     * Create an instance of {@link MapWrapper }
     * 
     */
    public MapWrapper createMapWrapper() {
        return new MapWrapper();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "FindByResponse")
    public JAXBElement<FindByResponse> createFindByResponse(FindByResponse value) {
        return new JAXBElement<FindByResponse>(_FindByResponse_QNAME, FindByResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "FindBy")
    public JAXBElement<FindBy> createFindBy(FindBy value) {
        return new JAXBElement<FindBy>(_FindBy_QNAME, FindBy.class, null, value);
    }

}
