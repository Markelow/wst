package com.ifmo.markeli.wst.Service; ;

import javax.xml.ws.Endpoint;

public class Application {
    public static void main(String... args) {
        String url = "http://0.0.0.0:8085/Books/Service";
        Endpoint.publish(url, new BooksService());
    }
}