package com.ifmo.markeli.wst.Service;

/**
 * Created by markeli on 01.05.17.
 */

import javax.xml.ws.Endpoint;

public class BooksPublisher
{
	public static void main(String[] args)
	{
		String url = "http://0.0.0.0:8085/Books/Service";
		Endpoint.publish(url, new BooksService());
	}
}
