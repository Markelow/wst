package com.ifmo.markeli.wst.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by markeli on 01.05.17.
 */
public class PostgresDAO
{
	PostgresDAO(Connection connection)
	{
		m_Connection = connection;
	}

	List<Book> FindBy(Map<String, String> parameters)
	{
		LinkedList<Book> result = new LinkedList<Book>();
		if (parameters.isEmpty())
		{
			return result;
		}

		try
		{
			Statement statement = m_Connection.createStatement();
			ArrayList<String> where_parameters = new ArrayList<String>();
			for (Map.Entry<String, String> entry: parameters.entrySet())
			{
				// XXX Escape strings!
				where_parameters.add(entry.getKey() + " = '" + entry.getValue() + "'");
			}

			String sql_request = "SELECT * FROM \"Books\" WHERE " + String.join(" AND ", where_parameters);
			boolean execute_result = statement.execute(sql_request);
			if (!execute_result)
			{
				Logger.getAnonymousLogger().log(Level.WARNING, "Failed execute sql request: \"" + sql_request + "\"");
			}

			ResultSet request_results = statement.getResultSet();
			while (request_results.next())
			{
				String name = request_results.getString("name");
				String description = request_results.getString("description");
				String author = request_results.getString("author");
				int isbn = request_results.getInt("isbn");
				result.add(new Book(name, description, author, isbn));
			}
		}
		catch (SQLException ex)
		{
			Logger.getAnonymousLogger().log(Level.WARNING, null, ex);
		}

		return result;
	}

	Connection m_Connection;
}
