/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markelow.rest.j2ee;

import com.markelow.rest.standalone.Book;
import com.markelow.rest.standalone.PostgreSQLDAO;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;

/**
 *
 * @author markelow
 */


@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
public class BookResource {

    @GET
    public List<Book> getPersonsByParams(@QueryParam("author") String author,
                                   @QueryParam("name") String name,
                                   @QueryParam("description") String description,
                                   @QueryParam("isbn") Integer isbn ) {
        String condition = "where ";
        if (isbn != null) {
            condition += "isbn=\'" + isbn.toString() + "\' and ";
        }
        if (name != null) {
            condition += "name=\'" + name + "\' and ";
        }
        if (author!= null) {
            condition += "surname=\'" + author + "\' and ";
        }
        if (description != null) {
            condition += "sex=\'" +description + "\' and ";
        }
        
        condition = condition.substring(0, condition.length() - 4);
        List<Book> books = new PostgreSQLDAO().getBooks(condition);
        return books;
    }
}
