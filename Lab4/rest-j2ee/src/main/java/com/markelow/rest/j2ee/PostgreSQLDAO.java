/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markelow.rest.j2ee;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author markelow
 */

public class PostgreSQLDAO {

    public List<Book> getBooks(String condition) {
        List<Book> books = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format("select * from books %s", condition));
            while (rs.next()) {
                String author = rs.getString("author");
                String name = rs.getString("name");
                String descirption = rs.getString("description");
                int isbn = rs.getInt("isbn");               

                Book person = new Book(author, name, descirption, isbn);
                books.add(person);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return books;
    }
}
