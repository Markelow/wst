CREATE TABLE "persons" (
 id bigserial NOT NULL,
 surname character varying(200),
 name character varying(200),
 midname character varying(200),
 age integer,
 CONSTRAINT "Persons_pkey" PRIMARY KEY (id)
);

INSERT INTO persons(surname, name, midname, age) values ('Petrov', 'Petr', 'Petrovich', 25);
INSERT INTO persons(surname, name, midname, age) values ('Ivanov', 'Vladimir', 'Ivanovich', 26);
INSERT INTO persons(surname, name, midname, age) values ('Bolshanina', 'Elizaveta', 'Petrovna', 27);
INSERT INTO persons(surname, name, midname, age) values ('Tikhomirov', 'Anotoliy', 'Vladimorovich', 28);
INSERT INTO persons(surname, name, midname, age) values ('Arshavin', 'Andrey', 'Sergeevich', 29);
INSERT INTO persons(surname, name, midname, age) values ('Burnaev', 'Dmitrii', 'Vladimorovich', 30);
INSERT INTO persons(surname, name, midname, age) values ('Zakharov', 'Vladislove', 'Aleekseevich', 31);
INSERT INTO persons(surname, name, midname, age) values ('Sidorov', 'Vyacheslav', 'Mikhailovich', 32);
INSERT INTO persons(surname, name, midname, age) values ('Ustugov', 'Sergei', 'Arnoldovich', 33);
INSERT INTO persons(surname, name, midname, age) values ('Kartsev', 'Ilya', 'Igorevich', 34);
INSERT INTO persons(surname, name, midname, age) values ('Krivoshapko', 'Nikita', 'Andreevich', 35);
