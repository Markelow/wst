package com.markelow.rest.client;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import java.awt.print.Book;
import java.util.List;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author markelow
 */

public class App {

    private static final String URL = "http://localhost:8080/rest/books";

    public static void main(String[] args) {
        Client client = Client.create();
        printList(getAllBooks(client, null));
        System.out.println();
        printList(getAllBooks(client, "markelow"));
    }

    private static List<Person> getAllBooks(Client client, String name) {
        WebResource webResource = client.resource(URL);
        if (name != null) {
            webResource = webResource.queryParam("name", name);
        }
        ClientResponse response
                = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException("Request failed");
        }
        GenericType<List<Book>> type = new GenericType<List<Book>>() {
        };
        return response.getEntity(type);
    }

    private static void printList(List<Book> boks) {
        for (Book person : boks) {
            System.out.println(person);
        }
    }
}


