package com.markelow.rest.client;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author markelow
 */
@XmlRootElement
public class Person {
    
    private String surname=null;
    private String name=null;
    private String midname=null;
    private Integer age=null;

    public Person() {
    }

    public Person(String surname, String name, String midname, int age) {
        this.surname = surname;
        this.name = name;
        this.midname = midname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }
    
    public String  getMidname(){
        return midname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public void setMidname(String midname){
        this.midname = midname;
    }

    @Override
    public String toString() {
        return "Person{" + "surname=" + surname + ", name=" + name + ",midname=" + midname + ", age=" + age + '}';
    }
}