/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markelow.rest.client;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author markelow
 */
@XmlRootElement
public class Book {
    
    private String author=null;
    private String name=null;
    private String descirption=null;
    private Integer isbn=null;

    public Book() {
    }

    public Book(String surname, String name, String midname, int age) {
        this.author = surname;
        this.name = name;
        this.descirption = midname;
        this.isbn = age;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public int getIsbn() {
        return isbn;
    }
    
    public String  getDescription(){
        return descirption;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setIsbn(int age) {
        this.isbn = age;
    }
    
    public void setDescription(String midname){
        this.descirption = midname;
    }

    @Override
    public String toString() {
        return "Person{" + "surname=" + author + ", name=" + name + ",midname=" + descirption + ", age=" + isbn + '}';
    }
}
