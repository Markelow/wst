package com.markelow.rest.standalone;

/**
 *
 * @author markelow
 */
public class NoParamException extends Exception {

    private static final long serialVersionUID = -6647544772732631047L;
    public static NoParamException DEFAULT_INSTANCE = new NoParamException("name cannot be null or empty");

    public NoParamException(String message) {
        super(message);
    }
}