package com.ifmo.markeli.wst.Client;

import com.ifmo.markeli.wst.Client.Generated.*;

import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by markeli on 01.05.17.
 */
public class Service
{
	Service(String host, int port) throws MalformedURLException
	{
		m_BaseServiceURL = "http://" + host + ":" + port + "/lab3/BooksService";
		URL wsdl_url = new URL(m_BaseServiceURL + "?wsdl");
		m_Service = new BooksService_Service(wsdl_url);
	}

	public List<Book> findBy(Map<String, String> parameters)
	{
		return getPort().findBy(Convert(parameters));
	}
	
	public boolean Create(Book book)
	{
		return getPort().create(book);
	}

	public boolean Update(int id, Map<String, String> fields_to_update)
	{
		return getPort().update(id, Convert(fields_to_update));
	}

	public boolean Remove(int id)
	{
		return getPort().delete(id);
	}

	private MapWrapper Convert(Map<String, String> target)
	{
		MapWrapper wrapper = new MapWrapper();
		for (Map.Entry<String, String> entry: target.entrySet())
		{
			MapWrapperEntry wrapper_entry = new MapWrapperEntry();
			wrapper_entry.setKey(entry.getKey());
			wrapper_entry.setValue(entry.getValue());
			wrapper.getEntry().add(wrapper_entry);
		}

		return wrapper;
	}

	private BooksService getPort()
	{
		BooksService port = m_Service.getBooksServicePort();
		BindingProvider bp = (BindingProvider)port;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, m_BaseServiceURL);
		return port;
	}

	private String m_BaseServiceURL;
	private BooksService_Service m_Service;
}
