package com.ifmo.markeli.wst.Service;

import javax.xml.ws.WebFault;

/**
 * Created by markeli on 1.05.17.
 */

@WebFault(faultBean = "com.ifmo.markeli.wst.BooksServiceException")
public class BooksServiceException extends Exception
{
	BooksServiceException(String message, BookServiceFaultBean fault, Throwable cause)
	{
		super(message, cause);
		m_fault_bean = fault;
	}

	BooksServiceException(String message, BookServiceFaultBean fault)
	{
		super(message);
		m_fault_bean = fault;
	}

	BookServiceFaultBean getFaultInfo()
	{
		return m_fault_bean;
	}

	private BookServiceFaultBean m_fault_bean;
}
