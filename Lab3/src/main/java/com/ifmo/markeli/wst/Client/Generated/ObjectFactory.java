
package com.ifmo.markeli.wst.Client.Generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ifmo.markeli.wst.Client.Generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
 
	private final static QName _Delete_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "Delete");
    private final static QName _UpdateResponse_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "UpdateResponse");
    private final static QName _DeleteResponse_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "DeleteResponse");
    private final static QName _Create_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "Create");
    private final static QName _CreateResponse_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "CreateResponse");
    private final static QName _FindByResponse_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "FindByResponse");
    private final static QName _FindByResponse_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "FindByResponse");
    private final static QName _FindBy_QNAME = new QName("http://Service.wst.markeli.ifmo.com/", "FindBy");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ifmo.markeli.wst.Client.Generated
     * 
     */
    public ObjectFactory() {
    }
	
    /**
     * Create an instance of {@link Delete }
     * 
     */
    public Delete createDelete() {
        return new Delete();
    }

    /**
     * Create an instance of {@link UpdateResponse }
     * 
     */
    public UpdateResponse createUpdateResponse() {
        return new UpdateResponse();
    }

    /**
     * Create an instance of {@link DeleteResponse }
     * 
     */
    public DeleteResponse createDeleteResponse() {
        return new DeleteResponse();
    }

    /**
     * Create an instance of {@link Create }
     * 
     */
    public Create createCreate() {
        return new Create();
    }

    /**
     * Create an instance of {@link CreateResponse }
     * 
     */
    public CreateResponse createCreateResponse() {
        return new CreateResponse();
    }

    /**
     * Create an instance of {@link FindByResponse }
     * 
     */
    public FindByResponse createFindByResponse() {
        return new FindByResponse();
    }

    /**
     * Create an instance of {@link Update }
     * 
     */
    public Update createUpdate() {
        return new Update();
    }

    /**
     * Create an instance of {@link FindBy }
     * 
     */
    public FindBy createFindBy() {
        return new FindBy();
    }

    /**
     * Create an instance of {@link FindByResponse }
     * 
     */
    public FindByResponse createFindByResponse() {
        return new FindByResponse();
    }

    /**
     * Create an instance of {@link FindBy }
     * 
     */
    public FindBy createFindBy() {
        return new FindBy();
    }

    /**
     * Create an instance of {@link MapWrapperEntry }
     * 
     */
    public MapWrapperEntry createMapWrapperEntry() {
        return new MapWrapperEntry();
    }

    /**
     * Create an instance of {@link Book }
     * 
     */
    public Book createBook() {
        return new Book();
    }

    /**
     * Create an instance of {@link MapWrapper }
     * 
     */
    public MapWrapper createMapWrapper() {
        return new MapWrapper();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Delete }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "Delete")
    public JAXBElement<Delete> createDelete(Delete value) {
        return new JAXBElement<Delete>(_Delete_QNAME, Delete.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "UpdateResponse")
    public JAXBElement<UpdateResponse> createUpdateResponse(UpdateResponse value) {
        return new JAXBElement<UpdateResponse>(_UpdateResponse_QNAME, UpdateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "DeleteResponse")
    public JAXBElement<DeleteResponse> createDeleteResponse(DeleteResponse value) {
        return new JAXBElement<DeleteResponse>(_DeleteResponse_QNAME, DeleteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Create }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "Create")
    public JAXBElement<Create> createCreate(Create value) {
        return new JAXBElement<Create>(_Create_QNAME, Create.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "CreateResponse")
    public JAXBElement<CreateResponse> createCreateResponse(CreateResponse value) {
        return new JAXBElement<CreateResponse>(_CreateResponse_QNAME, CreateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "FindByResponse")
    public JAXBElement<FindByResponse> createFindByResponse(FindByResponse value) {
        return new JAXBElement<FindByResponse>(_FindByResponse_QNAME, FindByResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Update }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "Update")
    public JAXBElement<Update> createUpdate(Update value) {
        return new JAXBElement<Update>(_Update_QNAME, Update.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindBy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Service.wst.markeli.ifmo.com/", name = "FindBy")
    public JAXBElement<FindBy> createFindBy(FindBy value) {
        return new JAXBElement<FindBy>(_FindBy_QNAME, FindBy.class, null, value);
    }

}
