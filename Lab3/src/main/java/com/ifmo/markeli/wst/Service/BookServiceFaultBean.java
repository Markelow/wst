package com.ifmo.markeli.wst.Service;

/**
 * Created by markeli on 01.05.17.
 */
public class BookServiceFaultBean
{
	BookServiceFaultBean(String message)
	{
		m_Message = message;
	}

	public String getMessage()
	{
		return m_Message;
	}

	private String m_Message;
}
