package com.ifmo.markeli.wst.Service; 


/**
 * Created by markeli on 01.05.17.
 */

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.sql.DataSource;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebService(serviceName = "BooksService")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public class BooksService
{
	@WebMethod(operationName = "FindBy")
	public List<Book> FindBy(@WebParam(name =  "parameters")@XmlJavaTypeAdapter(MapAdapter.class)HashMap<String, String> parameters) throws BooksServiceException
	{
		List<Book> books = null;
		if (parameters.isEmpty())
		{
			throw new BooksServiceException("Parameters are empty", new BookServiceFaultBean("FindBy method has empty parameters"));
		}
		
		try
		{
			PostgresDAO dao = new PostgresDAO(MakeConnection());
			books = dao.FindBy(parameters);
		}
		catch (SQLException ex)
		{
			Logger.getAnonymousLogger().log(Level.WARNING, null, ex);
		}

		return books;
	}

	
	@WebMethod(operationName = "Create")
	public boolean Create(@WebParam(name = "book")Book book) throws BooksServiceException
	{
		
		if (book.isEmpty())
		{
			throw new BooksServiceException("book is empty", new BookServiceFaultBean("Create method has illegal structure of Book to create"));
		}
		
		boolean result = false;
		try
		{
			PostgresDAO dao = new PostgresDAO(MakeConnection());
			result = dao.Create(book);
		}
		catch (SQLException ex)
		{
			Logger.getAnonymousLogger().log(Level.WARNING, null, ex);
			result = false;
		}

		return result;
	}

	@WebMethod(operationName = "Update")
	public boolean Update(@WebParam(name = "id")int id, @WebParam(name = "values")@XmlJavaTypeAdapter(MapAdapter.class)HashMap<String, String> values) throws BooksServiceException
	{
		if (values.isEmpty())
		{
			throw new BooksServiceException("Values to update are empty", new BookServiceFaultBean("Update method has empty values"));
		}
		
		boolean result = false;
		try
		{
			PostgresDAO dao = new PostgresDAO(MakeConnection());
			result = dao.Update(id, values);
		}
		catch (SQLException ex)
		{
			Logger.getAnonymousLogger().log(Level.WARNING, null, ex);
			result = false;
		}

		return result;
	}

	@WebMethod(operationName = "Delete")
	public boolean Delete(@WebParam(name = "id")int id)
	{
		boolean result = false;
		try
		{
			PostgresDAO dao = new PostgresDAO(MakeConnection());
			result = dao.Remove(id);
		}
		catch (SQLException ex)
		{
			Logger.getAnonymousLogger().log(Level.WARNING, null, ex);
			result = false;
		}

		return result;
	}
	
	Connection MakeConnection() throws SQLException
	{
		return m_DataSource.getConnection();
	}

	@Resource(mappedName = "java:/books")
	private DataSource m_DataSource;
}
