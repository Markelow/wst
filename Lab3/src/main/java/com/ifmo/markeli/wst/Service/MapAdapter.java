package com.ifmo.markeli.wst.Service;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by markeli on 01.05.17.
 */

public class MapAdapter extends XmlAdapter<MapWrapper, Map<String, String>>
{
	public Map<String, String> unmarshal(MapWrapper mapWrapper) throws Exception
	{
		HashMap<String, String> result = new HashMap<String, String>();
		for (MapWrapperEntry entry: mapWrapper.entry)
		{
			result.put(entry.key, entry.value);
		}

		return result;
	}

	public MapWrapper marshal(Map<String, String> stringStringMap) throws Exception
	{
		MapWrapper result = new MapWrapper();

		for (Map.Entry<String, String> entry: stringStringMap.entrySet())
		{
			MapWrapperEntry wrapper_entry = new MapWrapperEntry();
			wrapper_entry.key = entry.getKey();
			wrapper_entry.value = entry.getValue();

			result.entry.add(wrapper_entry);
		}

		return result;
	}
}
