package com.ifmo.markeli.wst.Client;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import com.ifmo.markeli.wst.Client.Generated.Book;
import com.ifmo.markeli.wst.Service.Book;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by markeli on 01.05.17.
 */
public class Arguments
{
	Arguments()
	{
		hostname = "127.0.0.1";
		port = 8080;
		parameters = new HashMap<String, String>();
		parameters.put("isbn", "1");
	}

	public boolean Parse(String[] args)
	{
		for (int i = 0; i < args.length; i++)
		{
			String argument = args[i];
			if ((argument.equals("-h") || argument.equals("--host")) && ++i < args.length )
			{
				hostname = args[i];
			}
			else if ((argument.equals("-p") || argument.equals("--port")) && ++i < args.length)
			{
				port = Integer.valueOf(args[i]);
			}
			else if ((argument.equals("-a") || argument.equals("--action")) && ++i < args.length)
			{
				action = args[i].toLowerCase();
			}
			else if ((argument.equals("-b") || argument.equals("--book")) && ++i < args.length)
			{
				Book book = new Book();
				for (String entry: args[i].split(";"))
				{
					String[] kv = entry.split("=");
					String key = kv[0].toLowerCase();
					String value = kv[1];
					if (key.equals("description"))
					{
						book.setDescription(value);
					}
					else if(key.equals("author"))
					{
						book.setAuthor(value);
					}
					else if (key.equals("isbn"))
					{
						book.setIsbn(Integer.valueOf(value));
					}
					else if (key.equals("name"))
					{
						book.setName(value);
					}
				}

				this.book = book;
			}
			else if ((argument.equals("-i") || argument.equals("--id")) && ++i < args.length)
			{
				desired_id = Integer.valueOf(args[i]);
			}
			else if ((argument.equals("-sp") || argument.equals("--search-parameters")) && ++i < args.length)
			{
				for (String entry: args[i].split(";"))
				{
					String[] kv = entry.split("=");
					parameters.put(kv[0].toLowerCase(), kv[1]);
				}
			}
		}

		return true;
	}

	public String hostname;
	public int port;
	public String action;
	public Book book;
	public int desired_id;
	public Map<String, String> parameters;
}
