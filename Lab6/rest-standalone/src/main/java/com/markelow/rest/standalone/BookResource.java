/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.markelow.rest.standalone.Book;
import com.markelow.rest.standalone.NoParamException;
import com.markelow.rest.standalone.PostgreSQLDAO;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;
import javax.ws.rs.DELETE;
import static javax.ws.rs.HttpMethod.POST;
import static javax.ws.rs.HttpMethod.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;

/**
 *
 * @author markelow
 */


@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
public class BookResource {

    @GET
    public List<Book> getPersonsByParams(@QueryParam("author") String author,
                                   @QueryParam("name") String name,
                                   @QueryParam("description") String description,
                                   @QueryParam("isbn") Integer isbn ) throws NoParamException {
        String condition = "where ";
        if (isbn != null) {
            condition += "isbn=\'" + isbn.toString() + "\' and ";
        }
        if (name != null) {
            condition += "name=\'" + name + "\' and ";
        }
        if (author!= null) {
            condition += "author=\'" + author + "\' and ";
        }
        
        if (condition == "where "){
            throw NoParamException.DEFAULT_INSTANCE;
        }
        condition = condition.substring(0, condition.length() - 4);
        List<Book> books = new PostgreSQLDAO().getBooks(condition);
        return books;
    }
    
    @PUT
    public int insertBook(
            @QueryParam("author") String author,
            @QueryParam("name") String name,
            @QueryParam("description") String description,
            @QueryParam("isbn") Integer isbn
            ) {
        Book book = new Book();
        if (author != null) {
            book.setAuthor(author);
        }
        if (name != null) {
            book.setName(name);
        }
        if (description != null) {
            book.setDescription(description);
        }
        if (isbn != null) {
            book.setIsbn(isbn);
        }
        
        PostgreSQLDAO dao = new PostgreSQLDAO();
        int exitStatus = dao.insertBook(book);
        return exitStatus;
    }

    @POST
    public int updateBook(
            @QueryParam("id") Integer id,
            @QueryParam("author") String author,
            @QueryParam("name") String name,
            @QueryParam("description") String description,
            @QueryParam("isbn") Integer isbn
            ) {
        PostgreSQLDAO dao = new PostgreSQLDAO();
        int exitStatus = dao.updateBook(id, name, author,description, isbn);
        return exitStatus;
    }

    @DELETE
    public int deleteBook(@QueryParam("id") Integer id) {
        PostgreSQLDAO dao = new PostgreSQLDAO();
        int exitStatus;
        exitStatus = dao.deleteBook(id);
        return exitStatus;
    }
}
