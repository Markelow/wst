package com.ifmo.markeli.wst.Service;

/**
 * Created by markeli on 05.03.17.
 */
public class Book
{
    public Book()
    {

    }

    public Book(String name, String description, String author, int isbn)
    {
        Name = name;
        Description = description;
        Author = author;
        Isbn = isbn;
    }

    public String getName()
    {
        return Name;
    }

    public void setName(String name)
    {
        Name = name;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String description)
    {
        Description = description;
    }

    public String getAuthor()
    {
        return Author;
    }

    public void setAuthor(String author)
    {
        Author = author;
    }

    public int getIsbn()
    {
        return Isbn;
    }

    public void setIsbn(int isbn)
    {
        Isbn = isbn;
    }

    private String Name;
    private String Description;
    private String Author;
    private int Isbn;
}
