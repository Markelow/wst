/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.markelow.rest.standalone;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.text.ParseException;

/**
 *
 * @author markelow
 */

public class PostgreSQLDAO {

    public List<Book> getBooks(String condition) {
        List<Book> books = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format("select * from books %s", condition));
            while (rs.next()) {
                String author = rs.getString("author");
                String name = rs.getString("name");
                String descirption = rs.getString("description");
                int isbn = rs.getInt("isbn");               

                Book person = new Book(author, name, descirption, isbn);
                books.add(person);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return books;
    }
    
    public Integer getId(Book book) {
        Integer id = null;
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement stmt = connection.createStatement();

            String condition = "where ";

            Field[] fields = book.getClass().getDeclaredFields();
            for (Field f : fields) {
                try {
                    f.setAccessible(true);
                    Class t = f.getType();
                    Object v = f.get(book);
                    String n = f.getName();
                    if (!t.isPrimitive() && v != null && !v.toString().equals("")) // found not default value
                    {
                        condition += String.format("%s=\'%s\' and ", n, v);
                    }
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger("ERR").log(Level.SEVERE, null, ex);
                }
            }

            condition = condition.substring(0, condition.length() - 4);

            ResultSet rs = stmt.executeQuery(String.format("select id from books %s", condition));

            rs.next();
            id = rs.getInt("id");

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return id;
    }

    public int insertBook(Book book) {
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement stmt = connection.createStatement();
            String updateLineFirstPart = "INSERT INTO books(";
            String updateLineSecondPart = " VALUES(";
            Field[] fields = book.getClass().getDeclaredFields();
            for (Field f : fields) {
                try {
                    f.setAccessible(true);
                    Class t = f.getType();
                    Object v = f.get(book);
                    String n = f.getName();
                    if (!t.isPrimitive() && v != null && !v.toString().equals("")) // found not default value
                    {
                        updateLineFirstPart += n + ",";
                        updateLineSecondPart += String.format("\'%s\',", v.toString());
                    }
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger("ERR").log(Level.SEVERE, null, ex);
                }
            }
            updateLineFirstPart = String.format("%s)", updateLineFirstPart.substring(0, updateLineFirstPart.length() - 1));
            updateLineSecondPart = String.format("%s)", updateLineSecondPart.substring(0, updateLineSecondPart.length() - 1));
            String updateQuery = updateLineFirstPart + updateLineSecondPart;
            int rs = stmt.executeUpdate(updateQuery);

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return getId(book);
    }

    public Integer updateBook(Integer id, String author, String name,String description, Integer isbn ) {
        Integer result = 0;
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "UPDATE books SET author = ?, name = ?, description = ?, isbn = ? WHERE id = ?");

            ps.setString(1, author);
            ps.setString(2, name);
            ps.setString(3, description);
            ps.setInt(4, isbn);
           

            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
            result = 1;
        }
        return result;
    }

    public int deleteBook(Integer id) {
        Integer result = 0;
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(
                    "DELETE from books WHERE id = ?");

            ps.setInt(1, id);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(PostgreSQLDAO.class.getName()).log(Level.SEVERE, null, ex);
            result = 1;
        }
        return result;
    }
}

