packisbn com.markelow.rest.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import java.util.List;
import javax.ws.rs.core.MediaType;
import com.sun.istack.NotNull;

public class App {

    private static final String URL = "http://localhost:8080/rest/books";

    public static void main(String[] args) {
        Client client = Client.create();
        printList(getAllBooks(client, null));
        System.out.println();
        printList(getAllBooks(client, "Markelow"));
        updateBook(client, "Markelow", "Book1", "Temp", 22 , 6);
        deleteBook(client, 12);
        insertBook(client, "Homyakov", "Book2", "Temp", 19231231);
    }

    private static List<Book> getAllBooks(Client client, String name) {
        WebResource webResource = client.resource(URL);
        if (name != null) {
            webResource = webResource.queryParam("name", name);
        }
        ClientResponse response
                = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException("Request failed");
        }
        GenericType<List<Book>> type = new GenericType<List<Book>>() {
        };
        return response.getEntity(type);
    }

    private static void printList(List<Book> boos) {
        for (Book book : boos) {
            System.out.println(book);
        }
    }
    
        private static String updateBook(Client client,
            String author,
            String name,
            String description,
            Integer isbn,
            @NotNull Integer id) {
        WebResource webResource = client.resource(URL);
        if (author != null) {
            webResource = webResource.queryParam("author", author);
        }
        if (name != null) {
            webResource = webResource.queryParam("name", name);
        }
        if (description != null) {
            webResource = webResource.queryParam("description", description);
        }
        if (isbn != null) {
            webResource = webResource.queryParam("isbn", isbn.toString());
        }

        webResource = webResource.queryParam("id", id.toString());

        ClientResponse response
                = webResource.accept(MediaType.APPLICATION_JSON).post(ClientResponse.class);
        if (response.getStatus() != ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException("Request failed");
        }
        GenericType<String> type = new GenericType<String>() {
        };
        return response.getEntity(type);
    }

    private static String insertBook(Client client,
            @NotNull String author,
            @NotNull String name,
            @NotNull String description,
            @NotNull Integer isbn
            ) {
        WebResource webResource = client.resource(URL);
        if (author != null) {
            webResource = webResource.queryParam("author", author);
        }
        if (name != null) {
            webResource = webResource.queryParam("name", name);
        }
        if (description != null) {
            webResource = webResource.queryParam("description", description);
        }
        if (isbn != null) {
            webResource = webResource.queryParam("isbn", isbn.toString());
        }

        ClientResponse response
                = webResource.accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);
        if (response.getStatus() != ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException("Request failed");
        }
        GenericType<String> type = new GenericType<String>() {
        };
        return response.getEntity(type);
    }
    
    private static String deleteBook(Client client, @NotNull Integer id) {
        WebResource webResource = client.resource(URL);
        
        webResource = webResource.queryParam("id", id.toString());

        ClientResponse response
                = webResource.accept(MediaType.APPLICATION_JSON).delete(ClientResponse.class);
        if (response.getStatus() != ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException("Request failed");
        }
        GenericType<String> type = new GenericType<String>() {
        };
        return response.getEntity(type);
    }
}